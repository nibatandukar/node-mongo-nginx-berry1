FROM node as base

WORKDIR /app

COPY . /app

RUN npm install


###########################################
FROM node:10-alpine3.9

WORKDIR /app

COPY --from=base /app /app

EXPOSE 5000

CMD ["node" "server.js"]
####################################################
